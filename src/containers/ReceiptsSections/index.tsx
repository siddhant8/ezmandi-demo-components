import React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    table: {
      minWidth: 650,
    },

    formControl: {
      margin: theme.spacing(1),
      minWidth: "100%",
    },
  })
);

function ReceiptsSection() {
  const classes = useStyles();
  const [age, setAge] = React.useState("");

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setAge(event.target.value as string);
  };
  return (
    <Grid container direction="row" alignItems="center" spacing={2}>
      <Grid item xs={12}>
        <Typography variant="h4" noWrap>
          Receipts
        </Typography>
      </Grid>

      <Grid item xs={3}>
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel id="demo-simple-select-outlined-label">
            Bulk Action
          </InputLabel>
          <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={age}
            onChange={handleChange}
            label="Bulk Action"
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <TableContainer style={{ boxShadow: "none" }} component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell style={{ fontWeight: "bolder" }}>
                  PO Number
                </TableCell>
                <TableCell style={{ fontWeight: "bolder" }} align="center">
                  Supplier
                </TableCell>
                <TableCell style={{ fontWeight: "bolder" }} align="center">
                  Location
                </TableCell>
                <TableCell style={{ fontWeight: "bolder" }} align="center">
                  Quantity
                </TableCell>
                <TableCell style={{ fontWeight: "bolder" }} align="center">
                  Price
                </TableCell>
                <TableCell style={{ fontWeight: "bolder" }} align="center">
                  Due Date
                </TableCell>
                <TableCell style={{ fontWeight: "bolder" }} align="center">
                  Actions
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow key={row.poNum}>
                  <TableCell component="th" scope="row">
                    {row.poNum}
                  </TableCell>
                  <TableCell align="center">{row.supplier}</TableCell>
                  <TableCell align="center">{row.location}</TableCell>
                  <TableCell align="center">{row.qty}</TableCell>
                  <TableCell align="center">{row.paid}</TableCell>
                  <TableCell align="center">{row.dueDate}</TableCell>
                  <TableCell align="center">
                    <Button color="primary">Accept</Button>
                    {" | "}
                    <Button color="primary">Reject</Button>
                    <Button variant="contained" color="primary">
                      Generate Receipt
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  );
}

export default ReceiptsSection;

function createData(
  poNum: number,
  dueDate: string,
  supplier: string,
  qty: string,
  location: string,
  commodity: string,
  grossTotal: number,
  toPay: number,
  paid: number
) {
  return {
    poNum,
    commodity,
    qty,
    paid,
    location,
    toPay,
    grossTotal,
    dueDate,
    supplier,
  };
}

const rows = [
  createData(
    124,
    "12/11/20",
    "Darius",
    "250Kg",
    "Bengaluru",
    "Paddy, Maize",
    4500,
    3000,
    1500
  ),
  createData(
    126,
    "12/11/20",
    "Donald",
    "250Kg",
    "Bengaluru",
    "Milk, Banana …",
    4500,
    3000,
    1500
  ),
  createData(
    128,
    "12/11/20",
    "Alfred",
    "250Kg",
    "Bengaluru",
    "Nuts, Wheat",
    4500,
    3000,
    1500
  ),
  createData(
    129,
    "12/11/20",
    "Diddy",
    "250Kg",
    "Bengaluru",
    "Paddy, Wheat",
    4500,
    3000,
    1500
  ),
];
