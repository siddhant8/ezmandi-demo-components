import React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import SearchIcon from "@material-ui/icons/Search";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import Chip from "@material-ui/core/Chip";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      boxShadow: "none",
    },
    bullet: {
      display: "inline-block",
      margin: "0 2px",
      transform: "scale(0.8)",
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    configCard: {
      boxShadow: "none",
      padding: "1em",
    },
    table: {
      minWidth: 650,
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: "100%",
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  })
);

function AddSupplier() {
  const classes = useStyles();
  const [age, setAge] = React.useState("");

  const handleDelete = () => {
    console.info("You clicked the delete icon.");
  };

  const handleClick = () => {
    console.info("You clicked the Chip.");
  };
  return (
    <Grid container direction="row" alignItems="center" spacing={2}>
      <Grid item xs={12}>
        <Typography variant="h4" noWrap>
          Add Suppliers
        </Typography>
      </Grid>
      <Grid item xs={10}>
        <Grid container spacing={1} direction="row" alignItems="flex-end">
          <Grid item xs={1}>
            <SearchIcon />
          </Grid>
          <Grid item xs={6}>
            <TextField
              id="input-with-icon-grid"
              fullWidth
              label="Search"
              placeholder="Search by Supplier, Location"
            />
          </Grid>
        </Grid>
      </Grid>
      {[1, 2, 3].map((item, index) => (
        <Grid item xs={5}>
          <Card variant="outlined">
            <CardContent>
              <Grid
                container
                direction="row"
                alignItems="center"
                justify="center"
                spacing={2}
              >
                <Grid item xs={2}>
                  <CardMedia
                    component="img"
                    alt="Contemplative Reptile"
                    height="140"
                    image="https://images.unsplash.com/photo-1597320691074-e930812569cb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=751&q=80"
                    title="Contemplative Reptile"
                  />
                </Grid>
                <Grid item xs={6}>
                  <Typography variant="body1">Ankur Tiwari</Typography>
                  <Typography variant="body2">Manufacturer</Typography>
                  <Typography variant="caption" style={{ marginBottom: "2em" }}>
                    Banagalore
                  </Typography>
                  <Grid
                    container
                    direction="row"
                    alignItems="center"
                    justify="space-between"
                  >
                    <Chip
                      label="Paddy"
                      onClick={handleClick}
                      variant="outlined"
                    />
                    <Chip
                      label="Maize"
                      onClick={handleClick}
                      variant="outlined"
                    />
                    <Chip
                      label="Wheat"
                      onClick={handleClick}
                      variant="outlined"
                    />
                  </Grid>
                </Grid>
                <Grid item xs={4}>
                  <Button
                    variant="contained"
                    style={{ whiteSpace: "nowrap" }}
                    color="primary"
                    size="small"
                  >
                    Send Request
                  </Button>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      ))}
      <Grid item xs={12}></Grid>
    </Grid>
  );
}

export default AddSupplier;

function createData(
  name: string,
  location: string,
  commodity: string,
  nature: string
) {
  return { name, location, commodity, nature };
}

const rows = [
  createData("Henk Fortuin", "Karnataka", "Paddy, Maize", "Trader"),
  createData("Ice cream sandwich", "Rajasthan", "Milk, Banana …", "Broker"),
  createData("Eclair", "Uttarakhand", "Nuts, Wheat", "Manufacturer"),
  createData("Cupcake", "Karnataka", "Paddy, Wheat", "Trader"),
];
