import React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import SearchIcon from "@material-ui/icons/Search";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      boxShadow: "none",
    },
    bullet: {
      display: "inline-block",
      margin: "0 2px",
      transform: "scale(0.8)",
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    configCard: {
      boxShadow: "none",
      padding: "1em",
    },
    table: {
      minWidth: 650,
    },
  })
);

function CRMGroups() {
  const classes = useStyles();
  return (
    <Grid
      container
      direction="row"
      justify="space-between"
      alignItems="center"
      spacing={2}
    >
      <Grid item xs={7}>
        <Typography variant="h4" noWrap>
          Groups
        </Typography>
      </Grid>
      <Grid item xs={5}>
        <Grid container spacing={1} direction="row" alignItems="flex-end">
          <Grid item xs={1}>
            <SearchIcon />
          </Grid>
          <Grid item xs={11}>
            <TextField
              id="input-with-icon-grid"
              fullWidth
              label="Search"
              placeholder="Search by Supplier, Location"
            />
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={3}>
        <Button variant="outlined" color="primary">
          Create new Group
        </Button>
      </Grid>
      <Grid item xs={12}>
        <TableContainer
          style={{ boxShadow: "none", textAlign: "center" }}
          component={Paper}
        >
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell style={{ fontWeight: "bolder" }}>
                  Supplier Name
                </TableCell>
                <TableCell
                  style={{ fontWeight: "bolder", textAlign: "center" }}
                  align="right"
                >
                  Description
                </TableCell>
                <TableCell style={{ fontWeight: "bolder" }} align="right">
                  Commodity
                </TableCell>
                <TableCell style={{ fontWeight: "bolder" }} align="right">
                  Actions
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow key={row.name}>
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  <TableCell align="center">{row.desc}</TableCell>
                  <TableCell align="right">{row.commodity}</TableCell>
                  <TableCell align="right">
                    <Button color="secondary">Remove</Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  );
}

export default CRMGroups;

function createData(name: string, desc: string, commodity: string) {
  return { name, desc, commodity };
}

const rows = [
  createData(
    "Henk Fortuin",
    "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam …",
    "Paddy, Maize"
  ),
  createData(
    "Ice cream sandwich",
    "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam …",
    "Milk, Banana …"
  ),
  createData(
    "Eclair",
    "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam …",
    "Nuts, Wheat"
  ),
  createData("Cupcake", "Karnataka", "Paddy, Wheat"),
];
