import React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import SearchIcon from "@material-ui/icons/Search";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      boxShadow: "none",
    },
    bullet: {
      display: "inline-block",
      margin: "0 2px",
      transform: "scale(0.8)",
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    configCard: {
      boxShadow: "none",
      padding: "1em",
    },
    table: {
      minWidth: 650,
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: "100%",
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  })
);

function CRMSuppliers() {
  const classes = useStyles();
  const [age, setAge] = React.useState("");

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setAge(event.target.value as string);
  };
  return (
    <Grid container direction="row" alignItems="center" spacing={2}>
      <Grid item xs={7}>
        <Typography variant="h4" noWrap>
          Suppliers
        </Typography>
      </Grid>
      <Grid item xs={5}>
        <Grid container spacing={1} direction="row" alignItems="flex-end">
          <Grid item xs={1}>
            <SearchIcon />
          </Grid>
          <Grid item xs={11}>
            <TextField
              id="input-with-icon-grid"
              fullWidth
              label="Search"
              placeholder="Search by Supplier, Location"
            />
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={3}>
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel id="demo-simple-select-outlined-label">
            Add to Supplier Group
          </InputLabel>
          <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={age}
            onChange={handleChange}
            label="Add to Supplier Group"
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={3}>
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel id="demo-simple-select-outlined-label">
            Manage Request
          </InputLabel>
          <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={age}
            onChange={handleChange}
            label="Manage Request"
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={3}>
        <Link to="/CRM/suppliers/add">
          <Button variant="outlined" color="primary" size="large">
            Add New Supplier
          </Button>
        </Link>
      </Grid>
      <Grid item xs={12}>
        <TableContainer style={{ boxShadow: "none" }} component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell style={{ fontWeight: "bolder" }}>
                  Supplier Name
                </TableCell>
                <TableCell style={{ fontWeight: "bolder" }} align="center">
                  Location
                </TableCell>
                <TableCell style={{ fontWeight: "bolder" }} align="center">
                  Commodity
                </TableCell>
                <TableCell style={{ fontWeight: "bolder" }} align="center">
                  Nature of Business
                </TableCell>
                <TableCell style={{ fontWeight: "bolder" }} align="center">
                  Actions
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow key={row.name}>
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  <TableCell align="center">{row.location}</TableCell>
                  <TableCell align="center">{row.commodity}</TableCell>
                  <TableCell align="center">{row.nature}</TableCell>
                  <TableCell align="center">
                    <Button color="primary">Add to Group</Button>
                    <Button color="primary">View Details</Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  );
}

export default CRMSuppliers;

function createData(
  name: string,
  location: string,
  commodity: string,
  nature: string
) {
  return { name, location, commodity, nature };
}

const rows = [
  createData("Henk Fortuin", "Karnataka", "Paddy, Maize", "Trader"),
  createData("Ice cream sandwich", "Rajasthan", "Milk, Banana …", "Broker"),
  createData("Eclair", "Uttarakhand", "Nuts, Wheat", "Manufacturer"),
  createData("Cupcake", "Karnataka", "Paddy, Wheat", "Trader"),
];
