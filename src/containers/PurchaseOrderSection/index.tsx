import React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import SearchIcon from "@material-ui/icons/Search";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      boxShadow: "none",
    },
    bullet: {
      display: "inline-block",
      margin: "0 2px",
      transform: "scale(0.8)",
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    configCard: {
      boxShadow: "none",
      padding: "1em",
    },
    table: {
      minWidth: 650,
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: "100%",
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  })
);

function PurchaseOrder() {
  const classes = useStyles();
  const [age, setAge] = React.useState("");

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setAge(event.target.value as string);
  };
  return (
    <Grid container direction="row" alignItems="center" spacing={2}>
      <Grid item xs={7}>
        <Typography variant="h4" noWrap>
          Purchase Order
        </Typography>
      </Grid>
      <Grid item xs={5}>
        <Grid container spacing={1} direction="row" alignItems="flex-end">
          <Grid item xs={1}>
            <SearchIcon />
          </Grid>
          <Grid item xs={11}>
            <TextField
              id="input-with-icon-grid"
              fullWidth
              label="Search"
              placeholder="Search by Supplier, Location"
            />
          </Grid>
        </Grid>
      </Grid>
      {["Pending", "Approved"].map((item, index) => (
        <Grid item xs={12}>
          <Typography variant="h6" noWrap>
            {item}
          </Typography>
          <TableContainer style={{ boxShadow: "none" }} component={Paper}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell style={{ fontWeight: "bolder" }}>PO No.</TableCell>
                  <TableCell style={{ fontWeight: "bolder" }} align="center">
                    PO Date
                  </TableCell>
                  <TableCell style={{ fontWeight: "bolder" }} align="center">
                    Commodity
                  </TableCell>
                  <TableCell style={{ fontWeight: "bolder" }} align="center">
                    Supplier
                  </TableCell>
                  <TableCell style={{ fontWeight: "bolder" }} align="center">
                    Nature of Business
                  </TableCell>
                  <TableCell style={{ fontWeight: "bolder" }} align="center">
                    {index % 2 === 0 ? "Actions" : "Progress"}
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {rows.map((row) => (
                  <TableRow key={row.poNum}>
                    <TableCell component="th" scope="row">
                      {row.poNum}
                    </TableCell>
                    <TableCell align="center">{row.poDate}</TableCell>
                    <TableCell align="center">{row.commodity}</TableCell>
                    <TableCell align="center">{row.supplier}</TableCell>
                    <TableCell align="center">{row.location}</TableCell>
                    <TableCell align="center">
                      {index % 2 === 0 ? (
                        <>
                          <Button color="primary">Approve</Button>
                          <Button color="primary">Reject</Button>
                          <Button color="primary">View Details</Button>
                        </>
                      ) : (
                        <Button color="primary">View Details</Button>
                      )}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      ))}
    </Grid>
  );
}

export default PurchaseOrder;

function createData(
  poNum: number,
  poDate: string,
  supplier: string,
  location: string,
  commodity: string
) {
  return { poNum, supplier, location, commodity, poDate };
}

const rows = [
  createData(123, "12/11/22", "Darius", "Karnataka", "Paddy, Maize"),
  createData(125, "12/11/22", "Alfred", "Rajasthan", "Milk, Banana …"),
  createData(189, "12/11/22", "Woody", "Uttarakhand", "Nuts, Wheat"),
  createData(190, "12/11/22", "Jesse", "Karnataka", "Paddy, Wheat"),
];
