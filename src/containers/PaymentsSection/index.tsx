import React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import SearchIcon from "@material-ui/icons/Search";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      boxShadow: "none",
    },
    bullet: {
      display: "inline-block",
      margin: "0 2px",
      transform: "scale(0.8)",
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    configCard: {
      boxShadow: "none",
      padding: "1em",
    },
    table: {
      minWidth: 650,
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: "100%",
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  })
);

function PaymentsSection() {
  const classes = useStyles();
  const [age, setAge] = React.useState("");

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setAge(event.target.value as string);
  };
  return (
    <Grid container direction="row" alignItems="center" spacing={2}>
      <Grid item xs={12}>
        <Typography variant="h4" noWrap>
          Payments
        </Typography>
      </Grid>
      <Grid item xs={5}>
        <Grid container spacing={1} direction="row" alignItems="flex-end">
          <Grid item xs={1}>
            <SearchIcon />
          </Grid>
          <Grid item xs={11}>
            <TextField
              id="input-with-icon-grid"
              fullWidth
              label="Search"
              placeholder="Search by Supplier, Location"
            />
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <TableContainer style={{ boxShadow: "none" }} component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell style={{ fontWeight: "bolder" }}>
                  PO Number
                </TableCell>
                <TableCell style={{ fontWeight: "bolder" }} align="center">
                  Due Date
                </TableCell>
                <TableCell style={{ fontWeight: "bolder" }} align="center">
                  Commodity
                </TableCell>
                <TableCell style={{ fontWeight: "bolder" }} align="center">
                  Supplier
                </TableCell>
                <TableCell style={{ fontWeight: "bolder" }} align="center">
                  Gross Total
                </TableCell>
                <TableCell style={{ fontWeight: "bolder" }} align="center">
                  To Pay
                </TableCell>
                <TableCell style={{ fontWeight: "bolder" }} align="center">
                  Paid
                </TableCell>
                <TableCell style={{ fontWeight: "bolder" }} align="center">
                  Action
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row) => (
                <TableRow key={row.poNum}>
                  <TableCell component="th" scope="row">
                    {row.poNum}
                  </TableCell>
                  <TableCell align="center">{row.dueDate}</TableCell>
                  <TableCell align="center">{row.commodity}</TableCell>
                  <TableCell align="center">{row.supplier}</TableCell>
                  <TableCell align="center">{row.grossTotal}</TableCell>
                  <TableCell align="center">{row.toPay}</TableCell>
                  <TableCell align="center">{row.paid}</TableCell>
                  <TableCell align="center">
                    <Button color="primary">View Details</Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  );
}

export default PaymentsSection;

function createData(
  poNum: number,
  dueDate: string,
  supplier: string,
  commodity: string,
  grossTotal: number,
  toPay: number,
  paid: number
) {
  return {
    poNum,
    commodity,
    paid,
    toPay,
    grossTotal,
    dueDate,
    supplier,
  };
}

const rows = [
  createData(124, "12/11/20", "Darius", "Paddy, Maize", 4500, 3000, 1500),
  createData(126, "12/11/20", "Donald", "Milk, Banana …", 4500, 3000, 1500),
  createData(128, "12/11/20", "Alfred", "Nuts, Wheat", 4500, 3000, 1500),
  createData(129, "12/11/20", "Diddy", "Paddy, Wheat", 4500, 3000, 1500),
];
