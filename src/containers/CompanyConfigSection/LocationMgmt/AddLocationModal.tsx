import React from "react";
import {
  makeStyles,
  Theme,
  createStyles,
  withStyles,
  WithStyles,
} from "@material-ui/core/styles";
import { useForm } from "react-hook-form";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });
export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}
const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});
const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& > *": {
        margin: theme.spacing(1),
      },
    },
    input: {
      display: "none",
    },

    container: {
      display: "flex",
      flexWrap: "wrap",
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
    },
  })
);

type FormData = {
  nickName: string;
  legalName: string;
  billingAddress: string;
  phoneNum: string;
  fromTime: string;
  toTime: string;
};

export default function AddLocationModal({
  isOpen,
  showModal,
}: AddLocationModalType) {
  // eslint-disable-next-line
  const { register, setValue, handleSubmit, errors } = useForm<FormData>();
  const onSubmit = handleSubmit(({ nickName }) => {
    console.log(nickName);
  });
  const classes = useStyles();

  return (
    <Dialog
      onClose={() => showModal(false)}
      aria-labelledby="customized-dialog-title"
      open={isOpen}
    >
      <DialogTitle
        id="customized-dialog-title"
        onClose={() => showModal(false)}
      >
        Add Address
      </DialogTitle>
      <form onSubmit={onSubmit}>
        <DialogContent dividers>
          <Grid container direction="row" justify="center" spacing={2}>
            {formFields.map((field) => {
              switch (field.type) {
                case "date":
                  return (
                    <Grid item xs={6}>
                      <TextField
                        id="time"
                        label={field.label}
                        type="time"
                        defaultValue="07:30"
                        className={classes.textField}
                        fullWidth
                        InputLabelProps={{
                          shrink: true,
                        }}
                        inputProps={{
                          step: 300, // 5 min
                        }}
                      />
                    </Grid>
                  );
                default:
                  return (
                    <Grid item xs={12}>
                      <TextField
                        id="outlined-full-width"
                        label={field.label}
                        style={{ margin: 8 }}
                        placeholder={field.placeholder}
                        fullWidth
                        margin="normal"
                        InputLabelProps={{
                          shrink: true,
                        }}
                        type={field.type}
                        variant="outlined"
                        size="small"
                        name={field.name}
                        ref={register}
                      />
                    </Grid>
                  );
              }
            })}
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => showModal(false)} variant="contained">
            Cancel
          </Button>
          <Button type="button" variant="contained" color="primary">
            Save changes
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
}

interface AddLocationModalType {
  isOpen: boolean;
  showModal: (x: boolean) => void;
}

interface formField {
  label: string;
  type: string;
  name: string;
  placeholder?: string;
  options?: string[];
}
const formFields: formField[] = [
  {
    label: "Nickname",
    name: "nickName",
    type: "string",
    placeholder: "Enter Nickname",
  },
  {
    label: "Legal Name",
    name: "legalName",
    type: "string",
    placeholder: "Enter Legal Name",
  },
  {
    label: "Billing Address",
    name: "billingAddress",
    type: "string",
    placeholder: "Enter Billing Address",
  },
  {
    label: "Phone Number",
    name: "phoneNum",
    type: "number",
    placeholder: "Enter Phone Number",
  },

  {
    label: "Working Hours (from)",
    name: "fromTime",
    type: "date",
    placeholder: "From Date",
  },

  {
    label: "Working Hours (to)",
    name: "toTime",
    type: "date",
    placeholder: "To Date",
  },
];
