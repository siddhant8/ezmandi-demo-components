import React, { useState } from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import AddLocationModal from "./AddLocationModal";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      boxShadow: "none",
    },
    bullet: {
      display: "inline-block",
      margin: "0 2px",
      transform: "scale(0.8)",
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    configCard: {
      boxShadow: "none",
      padding: "2em",
      display: "flex",
      justifyContent: "center",
      textAlign: "center",
    },
  })
);

function LocationMgmt() {
  const classes = useStyles();
  const [open, showModal] = useState(false);
  return (
    <>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
        spacing={2}
      >
        <Grid item xs={4}>
          <Typography style={{ fontWeight: "bolder" }} variant="h5">
            Location Management
          </Typography>
        </Grid>

        <Grid item xs={8}>
          <Button
            onClick={() => showModal(true)}
            variant="outlined"
            color="primary"
            size="large"
          >
            Add New Address
          </Button>
        </Grid>
        <Grid item xs={4}>
          <Card style={{ padding: "0.5em" }} className={classes.configCard}>
            <CardContent>
              <Grid
                container
                direction="row"
                justify="space-between"
                spacing={2}
              >
                <Grid item xs={5}>
                  <Typography style={{ fontWeight: "bolder" }} variant="body1">
                    Billing Address
                  </Typography>
                </Grid>
                <Grid item xs={7}>
                  <Typography variant="body1">
                    Sy. No.103/17, Kengeri Hobli, Near-Mysore Road, Kumbalgodu,
                    Bengaluru, Karnataka 560059
                  </Typography>
                </Grid>
                <Grid item xs={5}>
                  <Typography style={{ fontWeight: "bolder" }} variant="body1">
                    Phone Number
                  </Typography>
                </Grid>
                <Grid item xs={7}>
                  <Typography variant="body1">012345678</Typography>
                </Grid>
                <Grid item xs={5}>
                  <Typography style={{ fontWeight: "bolder" }} variant="body1">
                    Working Hours
                  </Typography>
                </Grid>
                <Grid item xs={7}>
                  <Typography variant="body1">8:00 am to 6 pm</Typography>
                </Grid>

                <Grid item xs={12}>
                  <Button variant="contained" color="primary" size="large">
                    Edit Address
                  </Button>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
      <AddLocationModal isOpen={open} showModal={showModal} />
    </>
  );
}

export default LocationMgmt;
