import React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      boxShadow: "none",
    },
    bullet: {
      display: "inline-block",
      margin: "0 2px",
      transform: "scale(0.8)",
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    configCard: {
      boxShadow: "none",
      padding: "1em",
    },
  })
);

function CompanyConfigSection() {
  const classes = useStyles();
  return (
    <Grid
      container
      direction="row"
      justify="space-between"
      alignItems="center"
      spacing={2}
    >
      <Grid item xs={12}>
        <Typography variant="h4" noWrap>
          Company Config Section
        </Typography>
      </Grid>
      {companyContent.map((content) => (
        <Grid item xs={11}>
          <Card className={classes.configCard}>
            <CardContent>
              <Grid
                container
                direction="row"
                justify="space-between"
                alignItems="center"
              >
                <Grid item>
                  <Typography variant="h5">{content.title}</Typography>
                  <Typography variant="body1">{content.desc}</Typography>
                </Grid>
                <Grid item>
                  <Link to={`/company-config/${content.route}`}>
                    <ChevronRightIcon
                      style={{
                        fontSize: "4em",
                        color: "#0091FF",
                        cursor: "pointer",
                      }}
                    />
                  </Link>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
}

export default CompanyConfigSection;

const companyContent = [
  {
    title: "Company Profile",
    desc: "View your billing address, phone number, working hours",
    route: "profile",
  },
  {
    title: "Location Management",
    desc: "View your billing address, phone number, working hours.",
    route: "location-mgmt",
  },
  {
    title: "Commodity Management",
    desc: "Manange all your commodity listing",
    route: "commodity-mgmt",
  },
  {
    title: "Terms & Conditions",
    desc: "Manage your terms & conditions",
    route: "terms-and-conditions",
  },
  {
    title: "User Management",
    desc: "Manage your terms & conditions",
    route: "user-mgmt",
  },
];
