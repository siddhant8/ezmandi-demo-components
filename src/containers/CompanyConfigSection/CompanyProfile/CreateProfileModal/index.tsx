import React from "react";
import {
  makeStyles,
  Theme,
  createStyles,
  withStyles,
  WithStyles,
} from "@material-ui/core/styles";
import { useForm } from "react-hook-form";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import PhotoCamera from "@material-ui/icons/PhotoCamera";
import CloseIcon from "@material-ui/icons/Close";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Autocomplete from "@material-ui/lab/Autocomplete";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });
export interface DialogTitleProps extends WithStyles<typeof styles> {
  id: string;
  children: React.ReactNode;
  onClose: () => void;
}
const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});
const DialogContent = withStyles((theme: Theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& > *": {
        margin: theme.spacing(1),
      },
    },
    input: {
      display: "none",
    },
  })
);

type FormData = {
  name: string;
  address: string;
  registration_type: string;
  business_nature: string;
  industry: string;
  commodity: string;
  contact_person: number;
};

export default function TransitionsModal({
  isOpen,
  showModal,
}: ProfileModalType) {
  // eslint-disable-next-line
  const { register, setValue, handleSubmit, errors } = useForm<FormData>();
  const onSubmit = handleSubmit(({ name }) => {
    console.log(name);
  });
  const classes = useStyles();

  return (
    <Dialog
      onClose={() => showModal(false)}
      aria-labelledby="customized-dialog-title"
      open={isOpen}
    >
      <DialogTitle
        id="customized-dialog-title"
        onClose={() => showModal(false)}
      >
        Create Company Profile
      </DialogTitle>
      <form onSubmit={onSubmit}>
        <DialogContent dividers>
          <Grid container direction="row" justify="center" spacing={2}>
            {formFields.map((field) => {
              switch (field.type) {
                case "select":
                  return (
                    <Grid item xs={12}>
                      <Autocomplete
                        id="combo-box-demo"
                        style={{ margin: 8 }}
                        options={field.options || []}
                        getOptionLabel={(option) => option}
                        fullWidth
                        size="small"
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            label={field.label}
                            variant="outlined"
                            name={field.name}
                          />
                        )}
                        ref={register}
                      />
                    </Grid>
                  );
                case "mediaInput":
                  return (
                    <Grid item xs={4}>
                      <div className={classes.root} style={{ margin: "auto" }}>
                        <input
                          accept="image/*"
                          className={classes.input}
                          id="contained-button-file"
                          multiple
                          type="file"
                          ref={register}
                          name={field.name}
                        />
                        <label htmlFor="contained-button-file">
                          <Button
                            variant="contained"
                            color="primary"
                            component="span"
                          >
                            Upload
                          </Button>
                        </label>
                        <input
                          accept="image/*"
                          className={classes.input}
                          id="icon-button-file"
                          type="file"
                          name={field.name}
                          ref={register}
                        />
                        <label htmlFor="icon-button-file">
                          <IconButton
                            color="primary"
                            aria-label="upload picture"
                            component="span"
                          >
                            <PhotoCamera />
                          </IconButton>
                        </label>
                      </div>
                    </Grid>
                  );
                default:
                  return (
                    <Grid item xs={12}>
                      <TextField
                        id="outlined-full-width"
                        label={field.label}
                        style={{ margin: 8 }}
                        placeholder={field.placeholder}
                        fullWidth
                        margin="normal"
                        InputLabelProps={{
                          shrink: true,
                        }}
                        type={field.type}
                        variant="outlined"
                        size="small"
                        name={field.name}
                        ref={register}
                      />
                    </Grid>
                  );
              }
            })}
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => showModal(false)} variant="contained">
            Cancel
          </Button>
          <Button type="button" variant="contained" color="primary">
            Save changes
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
}

interface ProfileModalType {
  isOpen: boolean;
  showModal: (x: boolean) => void;
}

interface formField {
  label: string;
  type: string;
  name: string;
  placeholder?: string;
  options?: string[];
}
const formFields: formField[] = [
  {
    label: "Upload Image",
    type: "mediaInput",
    name: "upload_img",
  },
  {
    label: "Name",
    name: "name",
    type: "string",
    placeholder: "Enter Name",
  },
  {
    label: "Address",
    name: "address",
    type: "string",
    placeholder: "Enter Address",
  },
  {
    label: "Type of Registration",
    name: "registration_type",
    type: "select",
    placeholder: "Select",
    options: ["Direct", "Indirect"],
  },
  {
    label: "Nature of Business",
    name: "business_nature",
    type: "select",
    placeholder: "Select",
    options: ["Normal", "Abnormal"],
  },
  {
    label: "Industry",
    name: "industry",
    type: "select",
    placeholder: "Select",
    options: ["Tech", "Finance"],
  },
  {
    label: "Commodity",
    name: "commodity",
    type: "select",
    placeholder: "Select",
    options: ["Rich", "Poor"],
  },
  {
    label: "Contact Person",
    name: "contact_person",
    type: "number",
    placeholder: "Enter Person's Contact",
  },
];
