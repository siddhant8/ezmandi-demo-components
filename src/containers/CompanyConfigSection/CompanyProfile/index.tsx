import React, { useState } from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import AddIcon from "@material-ui/icons/Add";
import Grid from "@material-ui/core/Grid";
import CreateProfileModal from "./CreateProfileModal";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      boxShadow: "none",
    },
    bullet: {
      display: "inline-block",
      margin: "0 2px",
      transform: "scale(0.8)",
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    configCard: {
      boxShadow: "none",
      padding: "2em",
      display: "flex",
      justifyContent: "center",
      textAlign: "center",
    },
  })
);

function CompanyProfile() {
  const classes = useStyles();
  const [open, showModal] = useState(false);
  return (
    <>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
        spacing={2}
      >
        <Grid item xs={3}>
          <Card className={classes.configCard}>
            <CardContent>
              <AddIcon
                onClick={() => showModal(true)}
                style={{
                  fontSize: "10em",
                  color: "#0091FF",
                  cursor: "pointer",
                }}
              />
              <Typography variant="body1">Create Company Profile</Typography>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
      <CreateProfileModal isOpen={open} showModal={showModal} />
    </>
  );
}

export default CompanyProfile;
