import React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import { Switch, Route } from "react-router-dom";
import Navbar from "../../components/Navbar";
import SideDrawer from "../../components/SideDrawer";
import Toolbar from "@material-ui/core/Toolbar";
import CssBaseline from "@material-ui/core/CssBaseline";
import DashboardSection from "../DashboardSection";
import CompanyConfigSection from "../CompanyConfigSection";
import CompanyProfile from "../CompanyConfigSection/CompanyProfile";
import CRMGroups from "../CRMSection/CRMGroups";
import CRMSuppliers from "../CRMSection/CRMSuppliers";
import AddSupplier from "../CRMSection/AddSupplier";
import PaymentsSection from "../PaymentsSection";
import ReportsSection from "../ReportsSection";
import ReceiptsSection from "../ReceiptsSections";
import PurchaseOrderSection from "../PurchaseOrderSection";
import LocationMgmt from "../CompanyConfigSection/LocationMgmt";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      fontFamily: `'SFProRegular'`,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
      backgroundColor: "#F0F2F5",
      minHeight: "100vh",
    },
  })
);

function App() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Navbar />
      <SideDrawer />
      <main className={classes.content}>
        <Toolbar />
        <Switch>
          <Route exact path="/" component={DashboardSection} />
          <Route path="/company-config" component={CompanyConfig} />
          <Route path="/CRM" component={CRMSection} />
          <Route path="/payments" component={PaymentsSection} />
          <Route path="/reports-centre" component={ReportsSection} />
          <Route path="/receipts" component={ReceiptsSection} />
          <Route path="/purchase-order" component={PurchaseOrderSection} />
        </Switch>
      </main>
    </div>
  );
}

export default App;

function CompanyConfig({ match }: urlParams) {
  return (
    <div>
      <Route exact path={`${match.url}`} component={CompanyConfigSection} />
      <Route path={`${match.url}/profile`} component={CompanyProfile} />
      <Route path={`${match.url}/location-mgmt`} component={LocationMgmt} />
    </div>
  );
}
function CRMSection({ match }: urlParams) {
  return (
    <div>
      <Route exact path={`${match.url}/suppliers`} component={CRMSuppliers} />
      <Route path={`${match.url}/suppliers/add`} component={AddSupplier} />
      <Route path={`${match.url}/groups`} component={CRMGroups} />
    </div>
  );
}

interface urlParams {
  match: {
    [key: string]: matchUrlObject;
  };
}

interface matchUrlObject {
  url: string;
  params: object;
  path: string;
}
