import React from "react";
import Typography from "@material-ui/core/Typography";

function DashboardSection() {
  return (
    <Typography variant="h1" noWrap>
      Clipped drawer
    </Typography>
  );
}

export default DashboardSection;
