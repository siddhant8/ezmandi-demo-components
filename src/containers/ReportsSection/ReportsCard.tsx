import React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import Grid from "@material-ui/core/Grid";
import CardContent from "@material-ui/core/CardContent";
import GetAppIcon from "@material-ui/icons/GetApp";
import InfoIcon from "@material-ui/icons/Info";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      padding: "2em",
    },
    pos: {
      color: "#009BDE",
    },
    iconSize: {
      fontSize: "1.3em",
    },
    downloadIcon: {
      color: "#009BDE",
      fontSize: "1.3em",
    },
  })
);

function ReportsCard() {
  const classes = useStyles();

  return (
    <Card variant="outlined">
      <CardContent>
        <Grid
          container
          direction="column"
          alignItems="center"
          justify="space-around"
          spacing={2}
        >
          <Grid item>
            <Typography className={classes.title} variant="h5">
              Title
            </Typography>
          </Grid>
          <Grid item>
            <Grid
              container
              direction="row"
              alignItems="center"
              justify="center"
            >
              <Grid item xs={4}>
                <GetAppIcon
                  className={classes.downloadIcon}
                  style={{ float: "right" }}
                />
              </Grid>
              <Grid item xs={8}>
                <Typography
                  className={classes.pos}
                  variant="body2"
                  color="textSecondary"
                >
                  Download Report
                </Typography>
              </Grid>
              <Grid item xs={1}>
                <InfoIcon className={classes.iconSize} />
              </Grid>
              <Grid item xs={11}>
                <Typography
                  variant="caption"
                  style={{ fontSize: "0.6rem", padding: "0.5em" }}
                  color="textSecondary"
                >
                  Report will be dowloaded in xlx format
                </Typography>
              </Grid>
              <Grid item xs={11}>
                <Typography variant="overline" color="textSecondary">
                  PURCHASE ORDER REPORT
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
}

export default ReportsCard;
