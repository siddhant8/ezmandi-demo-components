import React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import CreateCard from "../../components/CreateCard";
import ReportsCard from "./ReportsCard";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    configCard: {
      boxShadow: "none",
      padding: "1em",
    },
    table: {
      minWidth: 650,
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: "100%",
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  })
);

function CRMSuppliers() {
  const classes = useStyles();
  const [age, setAge] = React.useState("");

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setAge(event.target.value as string);
  };
  return (
    <Grid container direction="row" alignItems="center" spacing={2}>
      <Grid item xs={12}>
        <Typography variant="h4" noWrap>
          Reports Centre
        </Typography>
      </Grid>
      <Grid item xs={3}>
        <CreateCard />
      </Grid>
      {[1, 2, 3, 4].map((item) => (
        <Grid item xs={3}>
          <ReportsCard />
        </Grid>
      ))}
    </Grid>
  );
}

export default CRMSuppliers;

function createData(
  poNum: number,
  dueDate: string,
  supplier: string,
  commodity: string,
  grossTotal: number,
  toPay: number,
  paid: number
) {
  return {
    poNum,
    commodity,
    paid,
    toPay,
    grossTotal,
    dueDate,
    supplier,
  };
}

const rows = [
  createData(124, "12/11/20", "Darius", "Paddy, Maize", 4500, 3000, 1500),
  createData(126, "12/11/20", "Donald", "Milk, Banana …", 4500, 3000, 1500),
  createData(128, "12/11/20", "Alfred", "Nuts, Wheat", 4500, 3000, 1500),
  createData(129, "12/11/20", "Diddy", "Paddy, Wheat", 4500, 3000, 1500),
];
