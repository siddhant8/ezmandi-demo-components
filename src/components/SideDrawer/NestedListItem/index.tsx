import React, { useState } from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import Collapse from "@material-ui/core/Collapse";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { ListItemLink, LinkedItem } from "../ListItemLink";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import InboxIcon from "@material-ui/icons/MoveToInbox";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    nested: {
      paddingLeft: theme.spacing(4),
    },
    listItemText: {
      fontSize: "1em",
      overflow: "hidden",
    },
    listStyle: {
      backgroundColor: "rgb(235 235 235)",
      color: "#000",
      borderBottom: "0.5px solid #fff",
    },
    linkForListItem: {
      "&:hover": {
        color: "#000",
        textDecoration: "none",
      },
    },
  })
);

export interface NestedItem {
  title: string;
  route: string;
  type: string;
  nestedItems?: LinkedItem[];
}
interface NestedListItemProps {
  nestedList: NestedItem;
  index: number;
}

export function NestedListItem({ nestedList, index }: NestedListItemProps) {
  const classes = useStyles();
  const [open, showNestedList] = useState(false);

  const handleClick = () => {
    showNestedList(!open);
  };
  return (
    <>
      <Link className={classes.linkForListItem} to={nestedList.route}>
        <ListItem button className={classes.listStyle}>
          <ListItemIcon>
            <InboxIcon />
          </ListItemIcon>
          <ListItemText
            primary={nestedList.title}
            className={classes.listItemText}
          />
          {open ? (
            <ExpandLess onClick={handleClick} />
          ) : (
            <ExpandMore onClick={handleClick} />
          )}
        </ListItem>
      </Link>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          {nestedList.nestedItems &&
            nestedList.nestedItems.length > 0 &&
            nestedList.nestedItems.map((item: LinkedItem, index) => (
              <ListItemLink section={item} index={index} />
            ))}
        </List>
      </Collapse>
    </>
  );
}
