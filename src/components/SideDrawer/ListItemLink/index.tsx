import React, { useState } from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    nested: {
      paddingLeft: theme.spacing(4),
    },
    listItemText: {
      fontSize: "1em",
      overflow: "hidden",
    },
    listStyle: {
      backgroundColor: "rgb(235 235 235)",
      color: "#000",
      borderBottom: "0.5px solid #fff",
    },
    linkForListItem: {
      "&:hover": {
        color: "#000",
        textDecoration: "none",
      },
    },
  })
);

export interface LinkedItem {
  route: string;
  title: string;
}

interface ListItemLinkProps {
  section: LinkedItem;
  index: number;
}
export function ListItemLink({ section, index }: ListItemLinkProps) {
  const classes = useStyles();
  return (
    <Link className={classes.linkForListItem} to={section.route}>
      <ListItem className={classes.listStyle} button key={section.title}>
        <ListItemIcon>
          {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
        </ListItemIcon>
        <ListItemText
          className={classes.listItemText}
          primary={section.title}
        />
      </ListItem>
    </Link>
  );
}
