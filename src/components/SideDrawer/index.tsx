import React, { useState } from "react";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Toolbar from "@material-ui/core/Toolbar";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { ListItemLink } from "./ListItemLink";
import { NestedListItem } from "./NestedListItem";
import clsx from "clsx";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";

const drawerWidth = 240;

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: "nowrap",
    },
    drawerPaper: {
      width: drawerWidth,
    },
    drawerContainer: {
      overflow: "auto",
    },
    hide: {
      display: "none",
    },
    drawerOpen: {
      width: drawerWidth,
      borderRight: "none",
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
      backgroundColor: "#D8D8D8",
    },
    drawerClose: {
      borderRight: "none",
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: "hidden",
      width: theme.spacing(7) + 1,
      [theme.breakpoints.up("sm")]: {
        width: theme.spacing(9) + 1,
      },
      backgroundColor: "#D8D8D8",
    },
  })
);

function SideDrawer() {
  const customClasses = useStyles();
  const [drawer, toggleDrawer] = useState(true);

  console.log(customClasses.drawer);

  return (
    <Drawer
      variant="permanent"
      className={clsx(customClasses.drawer, {
        [customClasses.drawerOpen]: drawer,
        [customClasses.drawerClose]: !drawer,
      })}
      classes={{
        paper: clsx({
          [customClasses.drawerOpen]: drawer,
          [customClasses.drawerClose]: !drawer,
        }),
      }}
    >
      <Toolbar />
      <List>
        {sideDrawerSections.map((section, index) => {
          if (section.type === "link")
            return <ListItemLink section={section} index={index} />;
          else if (section.type === "nested")
            return <NestedListItem nestedList={section} index={index} />;
          // eslint-disable-next-line
        })}

        <ListItem button key={"collapse"} onClick={() => toggleDrawer(!drawer)}>
          <ListItemIcon>
            {!drawer ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </ListItemIcon>
          <ListItemText primary={"Collapse"} />
        </ListItem>
      </List>
    </Drawer>
  );
}

export default SideDrawer;

const sideDrawerSections = [
  { title: "Dashboard", route: "/", type: "link" },
  {
    title: "CRM",
    route: "/CRM/groups",
    type: "nested",
    nestedItems: [
      { title: "Groups", route: "/CRM/groups", type: "link" },
      { title: "Suppliers", route: "/CRM/suppliers", type: "link" },
    ],
  },
  { title: "Auction Floor", route: "/auction-floor", type: "link" },
  { title: "Receipt", route: "/receipts", type: "link" },
  { title: "Payment", route: "/payments", type: "link" },
  { title: "Purchase Order", route: "/purchase-order", type: "link" },
  { title: "Reports Centre", route: "/reports-centre", type: "link" },
  { title: "Company Configuration", route: "/company-config", type: "link" },
];
