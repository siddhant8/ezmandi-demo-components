import React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import Toolbar from "@material-ui/core/Toolbar";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";
import NotificationsNoneIcon from "@material-ui/icons/NotificationsNone";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      boxShadow: "none",
      backgroundColor: "#fff",
      color: "#000000",
    },
    logoNav: {
      backgroundColor: "#D8D8D8",
      height: "100%",
      color: "#000",
      padding: "1em 3.8em",
    },
    customToolbar: {
      paddingLeft: "0",
      justifyContent: "space-between",
    },
    iconStyle: {
      marginLeft: "3em",
    },
  })
);

function Navbar() {
  const classes = useStyles();
  return (
    <AppBar position="fixed" className={classes.appBar}>
      <Toolbar className={classes.customToolbar}>
        <Typography className={classes.logoNav} variant="h6" noWrap>
          EZ Mandi
        </Typography>
        <div>
          <NotificationsNoneIcon className={classes.iconStyle} />
          <HelpOutlineIcon className={classes.iconStyle} />
          <AccountCircleIcon className={classes.iconStyle} />
        </div>
      </Toolbar>
    </AppBar>
  );
}

export default Navbar;
