import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      boxShadow: "none",
      backgroundColor: "#fff",
      color: "#000000",
    },
    logoNav: {
      backgroundColor: "#D8D8D8",
      height: "100%",
      color: "#000",
      padding: "1em 3.8em",
    },
    customToolbar: {
      paddingLeft: "0",
      justifyContent: "space-between",
    },
    iconStyle: {
      marginLeft: "3em",
    },
  })
);
