import React from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import Grid from "@material-ui/core/Grid";
import CardContent from "@material-ui/core/CardContent";
import AddIcon from "@material-ui/icons/Add";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: "3.5em 1em",
    },
    addIcon: {
      fontSize: "5em",
      color: "#009BDE",
    },
    anchorDisable: {
      "&:hover": {
        textDecoration: "none",
      },
    },
  })
);

function CreateCard() {
  const classes = useStyles();
  return (
    <Link className={classes.anchorDisable} to={"/reports/add"}>
      <Card variant="outlined" className={classes.root}>
        <CardContent>
          <Grid
            container
            direction="column"
            alignItems="center"
            justify="space-between"
            spacing={2}
          >
            <Grid item>
              <AddIcon className={classes.addIcon} />
            </Grid>
            <Grid item>
              <Typography variant="h6" color="textSecondary">
                Add New Report
              </Typography>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Link>
  );
}

export default CreateCard;
